// Override Settings
var bcSfFilterSettings = {
    general: {
        limit: 16,
        /* Optional */
        //loadProductFirst: true,
    }
};

// Declare Templates
var bcSfFilterTemplate = {
    'soldOutClass': 'sold-out',
    'saleClass': 'on-sale',
    'soldOutLabelHtml': '<div>' + bcSfFilterConfig.label.sold_out + '</div>',
    'saleLabelHtml': '<div>' + bcSfFilterConfig.label.sale + '</div>',
    'vendorHtml': '<div>{{itemVendorLabel}}</div>',

    // Grid Template
	/*
    'productGridItemHtml': '<div class="grid__item wide--one-fifth large--one-quarter medium-down--one-half">' +
                                '<div class="{{soldOutClass}} {{saleClass}}">' +
                                    '<a href="{{itemUrl}}" class="grid-link">' +
                                        '<span class="grid-link__image grid-link__image--product">' +
                                            '{{itemSaleLabel}}' +
                                            '{{itemSoldOutLabel}}' +
                                            '<span class="grid-link__image-centered"><img src="{{itemThumbUrl}}" alt="{{itemTitle}}" /></span>' +
                                        '</span>' +
                                        '<p class="grid-link__title">{{itemTitle}}</p>' +
                                        '{{itemVendor}}' +
                                        '{{itemPrice}}' +
                                    '</a>' +
                                '</div>' +
                            '</div>',
	*/
    'productGridItemHtml': 
		'<div class="item product col-sm-2 col-md-4 col-lg-4" style="">'+
		'<div class="whole-content">'+
			'<div class="product-action"> '+
				'<a href="{{itemUrl}}" class="product-image">'+
					'<img src="{{itemThumbUrl}}" alt="{{itemTitle}}" class="img-responsive main">'+
					'<img src="{{itemThumbUrl}}" alt="{{itemTitle}}" class="alt-img img-responsive">'+
				'</a> '+
			'</div>'+
		'<div class="product-content">'+
		  '<h3 class="product-name">'+
			'<a href="{{itemUrl}}" title="{{itemTitle}}">'+
			  '{{itemTitle}}'+
			'</a>'+
		  '</h3>'+
		  '<div class="product-short-desc">'+
			''+
			'</div>'+
		 '<div class="price price_collections">'+
			'<div class="compare-price">'+
				'<span class="money"></span>'+
			'</div>'+
			'<span class="money price11">'+
				'{{itemPrice}}'+
			'</span>'+
		 '</div>  '+
  
          '<div class="yotpo-container_bottom">' +
            '<div class="yotpo bottomLine" data-product-id="{{itemId}}"></div>' +
          '</div>' +

		 '<div class="product-more-action">'+
			'<a href="{{itemUrl}}" class="quick-view">'+
				'Quick View'+
			'</a>'+
			'<a href="{{itemUrl}}" class="more-info">'+
				'More Information'+
			'</a>'+
		 '</div>  '+

		'</div>'+
		'</div>',

    // Pagination Template
	/*
    'previousActiveHtml': '<li><a href="{{itemUrl}}">&larr;</a></li>',
    'previousDisabledHtml': '<li class="disabled"><span>&larr;</span></li>',
    'nextActiveHtml': '<li><a href="{{itemUrl}}">&rarr;</a></li>',
    'nextDisabledHtml': '<li class="disabled"><span>&rarr;</span></li>',
    'pageItemHtml': '<li><a href="{{itemUrl}}">{{itemTitle}}</a></li>',
    'pageItemSelectedHtml': '<li><span class="active">{{itemTitle}}</span></li>',
    'pageItemRemainHtml': '<li><span>{{itemTitle}}</span></li>',
    'paginateHtml': '<ul class="pagination-custom">{{previous}}{{pageItems}}{{next}}</ul>',
	 */
    'previousActiveHtml': '<span class="next"><a href="{{itemUrl}}" title=""><i class="fa fa-caret-left"></i></a></span>',
    'previousDisabledHtml': '',
    'nextActiveHtml': '<span class="next"><a href="{{itemUrl}}" title=""><i class="fa fa-caret-right"></i></a></span>',
    'nextDisabledHtml': '',
    'pageItemHtml': '<span class="page"><a href="{{itemUrl}}" title="">{{itemTitle}}</a></span>',
    'pageItemSelectedHtml': '<span class="page current">{{itemTitle}}</span>',
    'pageItemRemainHtml': '',
    'paginateHtml': '<div class="pagination">{{previous}}{{pageItems}}{{next}}</div>',
    // Sorting Template
    //'sortingHtml': '<label class="label--hidden">' + bcSfFilterConfig.label.sorting + '</label><select class="collection-sort__input">{{sortingItems}}</select>',
	'sortingHtml': '<select class="collection-sort__input">{{sortingItems}}</select>',
};

/************************** BUILD PRODUCT LIST **************************/

// Build Product Grid Item
BCSfFilter.prototype.buildProductGridItem = function(data, index) {
    /*** Prepare data ***/
    var images = data.images_info;
    data.price_min *= 100, data.price_max *= 100, data.compare_at_price_min *= 100, data.compare_at_price_max *= 100; // Displaying price base on the policy of Shopify, have to multiple by 100
    var soldOut = !data.available; // Check a product is out of stock
    var onSale = data.compare_at_price_min > data.price_min; // Check a product is on sale
    var priceVaries = data.price_min != data.price_max; // Check a product has many prices
    // Get First Variant (selected_or_first_available_variant)
    var firstVariant = data['variants'][0];
    if (getParam('variant') !== null && getParam('variant') != '') {
        var paramVariant = data.variants.filter(function(e) { return e.id == getParam('variant'); });
        if (typeof paramVariant[0] !== 'undefined') firstVariant = paramVariant[0];
    } else {
        for (var i = 0; i < data['variants'].length; i++) {
            if (data['variants'][i].available) {
                firstVariant = data['variants'][i];
                break;
            }
        }
    }
    /*** End Prepare data ***/
	// console.log(data);
    // Get Template
    var itemHtml = bcSfFilterTemplate.productGridItemHtml;

	if (images.length > 1 ) {
		var itemThumbUrl2 = this.optimizeImage(images[1]['src']);
		itemHtml = itemHtml.replace(/<img src="{{itemThumbUrl}}" alt="{{itemTitle}}" class="alt-img img-responsive">/g, '<img src="'+itemThumbUrl2+'" alt="{{itemTitle}}" class="alt-img img-responsive">');
	} else {
		itemHtml = itemHtml.replace(/<img src="{{itemThumbUrl}}" alt="{{itemTitle}}" class="alt-img img-responsive">/g, '');
	}
	
    // Add Thumbnail
    var itemThumbUrl = images.length > 0 ? this.optimizeImage(images[0]['src']) : bcSfFilterConfig.general.no_image_url;
    itemHtml = itemHtml.replace(/{{itemThumbUrl}}/g, itemThumbUrl);
	
    // Add Price
    var itemPriceHtml = '';
    if (data.title != '')  {
        itemPriceHtml += '<p class="grid-link__meta">';
        if (priceVaries) {
            //itemPriceHtml += (bcSfFilterConfig.label.from_price).replace(/{{ price }}/g, this.formatMoney(data.price_min));
            itemPriceHtml += 'From ' + this.formatMoney(data.price_min);
        } else {
            itemPriceHtml += this.formatMoney(data.price_min);
        }
        if (onSale) {
            itemPriceHtml += '<s class="grid-link__sale_price">' + this.formatMoney(data.compare_at_price_min) + '</s> ';
        }
        itemPriceHtml += '</p>';
    }
    itemHtml = itemHtml.replace(/{{itemPrice}}/g, itemPriceHtml);

    // Add soldOut class
    var soldOutClass = soldOut ? bcSfFilterTemplate.soldOutClass : '';
    itemHtml = itemHtml.replace(/{{soldOutClass}}/g, soldOutClass);
  
    // Add onSale class
    var saleClass = onSale ? bcSfFilterTemplate.saleClass : '';
    itemHtml = itemHtml.replace(/{{saleClass}}/g, saleClass);
  
    // Add soldOut Label
    var itemSoldOutLabelHtml = soldOut ? bcSfFilterTemplate.soldOutLabelHtml : '';
    itemHtml = itemHtml.replace(/{{itemSoldOutLabel}}/g, itemSoldOutLabelHtml);

    // Add onSale Label
    var itemSaleLabelHtml = onSale ? bcSfFilterTemplate.saleLabelHtml : '';
    itemHtml = itemHtml.replace(/{{itemSaleLabel}}/g, itemSaleLabelHtml);

    // Add Vendor
    var itemVendorHtml = bcSfFilterConfig.custom.vendor_enable ? bcSfFilterTemplate.vendorHtml.replace(/{{itemVendorLabel}}/g, data.vendor) : '';
    itemHtml = itemHtml.replace(/{{itemVendor}}/g, itemVendorHtml);

    // Add main attribute (Always put at the end of this function)
    itemHtml = itemHtml.replace(/{{itemId}}/g, data.id);
    itemHtml = itemHtml.replace(/{{itemHandle}}/g, data.handle);
    itemHtml = itemHtml.replace(/{{itemTitle}}/g, data.title);
    itemHtml = itemHtml.replace(/{{itemUrl}}/g, this.buildProductItemUrl(data));

    return itemHtml;
};

// Build Product List Item
BCSfFilter.prototype.buildProductListItem = function(data) {
    // // Add Description
    // var itemDescription = jQ('<p>' + data.body_html + '</p>').text();
    // // Truncate by word
    // itemDescription = (itemDescription.split(" ")).length > 51 ? itemDescription.split(" ").splice(0, 51).join(" ") + '...' : itemDescription.split(" ").splice(0, 51).join(" ");
    // // Truncate by character
    // itemDescription = itemDescription.length > 350 ? itemDescription.substring(0, 350) + '...' : itemDescription.substring(0, 350);
    // itemHtml = itemHtml.replace(/{{itemDescription}}/g, itemDescription);
};

// Customize data to suit the data of Shopify API
BCSfFilter.prototype.prepareProductData = function(data) {
    for (var k in data) {
        // Featured image
        if (data['images_info'] > 0) {
            data[k]['featured_image'] = data['images_info'][0];
        } else {
            data[k]['featured_image'] = {width: '', height: '', aspect_ratio: 0}
        }

        // Add Options
        var optionsArr = [];
        for (var i in data[k]['options_with_values']) {
            optionsArr.push(data[k]['options_with_values'][i]['name']);
        }
        data[k]['options'] = optionsArr;

        // Customize variants
        for (var i in data[k]['variants']) {
            var variantOptionArr = [];
            var count = 1;
            var variant = data[k]['variants'][i];
            // Add Options
            var variantOptions = variant['merged_options'];
            if (Array.isArray(variantOptions)) {
                for (var j = 0; j < variantOptions.length; j++) {
                    var temp = variantOptions[j].split(':');
                    data[k]['variants'][i]['option' + (parseInt(j) + 1)] = temp[1];
                    data[k]['variants'][i]['option_' + temp[0]] = temp[1];
                    variantOptionArr.push(temp[1]);
                }
                data[k]['variants'][i]['options'] = variantOptionArr;
            }
            data[k]['variants'][i]['compare_at_price'] = parseFloat(data[k]['variants'][i]['compare_at_price']) * 100;
            data[k]['variants'][i]['price'] = parseFloat(data[k]['variants'][i]['price']) * 100;
        }

        // Add Description
        data[k]['description'] = data[k]['body_html'];
    }
    return data;
};

/************************** END BUILD PRODUCT LIST **************************/

// Build Pagination
BCSfFilter.prototype.buildPagination = function(totalProduct) {
    if (this.getSettingValue('general.paginationType') == 'default') {
        // Get page info
        var currentPage = parseInt(this.queryParams.page);
        var totalPage = Math.ceil(totalProduct / this.queryParams.limit);

        // If it has only one page, clear Pagination
        if (totalPage == 1) {
            jQ(this.selector.bottomPagination).html('');
            return false;
        }

        if (this.getSettingValue('general.paginationType') == 'default') {
            var paginationHtml = bcSfFilterTemplate.paginateHtml;

            // Build Previous
            var previousHtml = (currentPage > 1) ? bcSfFilterTemplate.previousActiveHtml : bcSfFilterTemplate.previousDisabledHtml;
            previousHtml = previousHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage - 1));
            paginationHtml = paginationHtml.replace(/{{previous}}/g, previousHtml);

            // Build Next
            var nextHtml = (currentPage < totalPage) ? bcSfFilterTemplate.nextActiveHtml :  bcSfFilterTemplate.nextDisabledHtml;
            nextHtml = nextHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage + 1));
            paginationHtml = paginationHtml.replace(/{{next}}/g, nextHtml);

            // Create page items array
            var beforeCurrentPageArr = [];
            for (var iBefore = currentPage - 1; iBefore > currentPage - 3 && iBefore > 0; iBefore--) {
                beforeCurrentPageArr.unshift(iBefore);
            }
            if (currentPage - 4 > 0) {
                beforeCurrentPageArr.unshift('...');
            }
            if (currentPage - 4 >= 0) {
                beforeCurrentPageArr.unshift(1);
            }
            beforeCurrentPageArr.push(currentPage);

            var afterCurrentPageArr = [];
            for (var iAfter = currentPage + 1; iAfter < currentPage + 3 && iAfter <= totalPage; iAfter++) {
                afterCurrentPageArr.push(iAfter);
            }
            if (currentPage + 3 < totalPage) {
                afterCurrentPageArr.push('...');
            }
            if (currentPage + 3 <= totalPage) {
                afterCurrentPageArr.push(totalPage);
            }

            // Build page items
            var pageItemsHtml = '';
            var pageArr = beforeCurrentPageArr.concat(afterCurrentPageArr);
            for (var iPage = 0; iPage < pageArr.length; iPage++) {
                if (pageArr[iPage] == '...') {
                    pageItemsHtml += bcSfFilterTemplate.pageItemRemainHtml;
                } else {
                    pageItemsHtml += (pageArr[iPage] == currentPage) ? bcSfFilterTemplate.pageItemSelectedHtml : bcSfFilterTemplate.pageItemHtml;
                }
                pageItemsHtml = pageItemsHtml.replace(/{{itemTitle}}/g, pageArr[iPage]);
                pageItemsHtml = pageItemsHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, pageArr[iPage]));
            }
            paginationHtml = paginationHtml.replace(/{{pageItems}}/g, pageItemsHtml);

            jQ(this.selector.bottomPagination).html(paginationHtml);
        }
    }
};

/************************** BUILD TOOLBAR **************************/

// Build Sorting
BCSfFilter.prototype.buildFilterSorting = function() {
    if (bcSfFilterTemplate.hasOwnProperty('sortingHtml')) {
        jQ(this.selector.topSorting).html('');

        var sortingArr = this.getSortingList();
        if (sortingArr) {
            // Build content 
            var sortingItemsHtml = '';
            for (var k in sortingArr) {
                sortingItemsHtml += '<option value="' + k +'">' + sortingArr[k] + '</option>';
            }
            var html = bcSfFilterTemplate.sortingHtml.replace(/{{sortingItems}}/g, sortingItemsHtml);
            jQ(this.selector.topSorting).html(html);

            // Set current value
            jQ(this.selector.topSorting + ' select').val(this.queryParams.sort);
        }
    }
};

// Build Display type (List / Grid / Collage)
// BCSfFilter.prototype.buildFilterDisplayType = function() {
//     var itemHtml = '<a href="' + this.buildToolbarLink('display', 'list', 'grid') + '" title="Grid view" class="change-view bc-sf-filter-display-grid" data-view="grid"><span class="icon-fallback-text"><i class="fa fa-th" aria-hidden="true"></i><span class="fallback-text">Grid view</span></span></a>';
//     itemHtml += '<a href="' + this.buildToolbarLink('display', 'grid', 'list') + '" title="List view" class="change-view bc-sf-filter-display-list" data-view="list"><span class="icon-fallback-text"><i class="fa fa-list" aria-hidden="true"></i><span class="fallback-text">List view</span></span></a>';
//     jQ(this.selector.topDisplayType).html(itemHtml);

//     // Active current display type
//     jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-list').removeClass('active');
//     jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-grid').removeClass('active');
//     if (this.queryParams.display == 'list') {
//         jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-list').addClass('active');
//     } else if (this.queryParams.display == 'grid') {
//         jQ(this.selector.topDisplayType).find('.bc-sf-filter-display-grid').addClass('active');
//     }
// };

/************************** END BUILD TOOLBAR **************************/

// Add additional feature for product list, used commonly in customizing product list
BCSfFilter.prototype.buildExtrasProductList = function(data, eventType) {};

// Build additional elements
BCSfFilter.prototype.buildAdditionalElements = function(data, eventType) {
  /*if(typeof Yotpo != 'undefined' && typeof yotpo != 'undefined'){
    var api = new Yotpo.API(yotpo);
    api.refreshWidgets();
  }  */
};

// Get Filter Data custom with infinite scoll history
BCSfFilter.prototype.getFilterData = function(a, b) {
    var b = void 0 !== b ? b : 0,
        c = this;
    this.showLoading(), this.beforeGetFilterData(a), this.prepareRequestParams(a), this.queryParams.callback = "BCSfFilterCallback", this.queryParams.event_type = a;
    var d = this.isSearchPage() ? this.getApiUrl("search") : this.getApiUrl("filter"),
        e = document.createElement("script");
    e.type = "text/javascript";
    var f = (new Date).getTime();
    e.src = d + "?t=" + f + "&" + jQ.param(this.queryParams), e.id = "bc-sf-filter-script", e.async = !0, e.addEventListener("error", function(a) {
        "function" == typeof document.getElementById(e.id).remove ? document.getElementById(e.id).remove() : document.getElementById(e.id).outerHTML = "", b < 3 ? (b++, c.getFilterData("resend", b)) : c.showError("Oops, we cannot fetch products at this moment. Please try again later.")
    }), document.getElementsByTagName("head")[0].appendChild(e), e.onload = function() {
        "function" == typeof document.getElementById(e.id).remove ? document.getElementById(e.id).remove() : document.getElementById(e.id).outerHTML = ""
    }
	// console.log(this.queryParams);
	if ( a == 'init') {
	} else {
		jQuery.cookie('bcsf_collection', this.queryParams.collection_scope, 1);
		jQuery.cookie('bcsf_page', this.queryParams.page, 1);
	}
}

BCSfFilter.prototype.buildProductItemUrl = function(a, b) {
    var b = void 0 === b || b;
    if (false) {
        if ("/" == window.location.pathname || this.isSearchPage() || this.isVendorPage()) return "/collections/all/products/" + a.handle;
        if (this.isTagPage()) {
            var c = window.location.pathname.split("/");
            return c.length >= 4 ? "/collections/" + c[2] + "/products/" + a.handle : "/collections/all/products/" + a.handle
        }
        var c = window.location.pathname.split("/");
        return void 0 !== c[2] ? "/collections/" + c[2] + "/products/" + a.handle : window.location.pathname + "/products/" + a.handle
    }
    return "/products/" + a.handle
}

BCSfFilter.prototype.buildAll = function(a, b, c) {
    !0 === b && a.hasOwnProperty("filter") && (this.buildFilterTree(a.filter.options), this.getSettingValue("general.showRefineBy") && this.buildFilterSelection(a), this.buildFilterTreeMobile(), this.buildFilterTreeMobileButton(a), this.buildAdditionalFilterEvent()), a.total_product > 0 && (this.buildProductList(a.products, c), "default" == this.getSettingValue("general.paginationType") ? this.buildPagination(a.total_product) : "load_more" == this.getSettingValue("general.paginationType") && this.buildLoadMoreButton(a.total_product), this.buildToolbar(), this.buildToolbarEvent(a), this.buildAdditionalElements(a, c), this.buildScrollToTop(), jQ(this.selector.filterWrapper).show(), jQ(this.selector.topNotification).length > 0 && jQ(this.selector.topNotification).html("")), "collection" == c && this.buildPageInfo(a), this.buildFilterStyle(), this.isSearchPage() && (this.buildSearchResultHeader(a), this.buildSearchResultNumber(a)), this.selectFilter = !1;
	jQuery.cookie('bcsf_total_product', a.total_product, 1);
}