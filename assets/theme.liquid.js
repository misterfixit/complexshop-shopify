  var pInfScrLoading = false;
  var pInfScrDelay = 100;

  function pInfScrExecute() {
    if($(document).height() - 800 < ($(document).scrollTop() + $(window).height())) {
      var loadingImage;
      pInfScrNode = $('.more').last(); 
      pInfScrURL = $('.more p a').last().attr("href");
      if(!pInfScrLoading && pInfScrNode.length > 0 && pInfScrNode.css('display') != 'none') {
        $.ajax({
          type: 'GET',
          url: pInfScrURL,
          beforeSend: function() {
            pInfScrLoading = true;
            loadingImage = pInfScrNode.clone().empty().append('<img src=\"http://cdn.shopify.com/s/files/1/0068/2162/assets/loading.gif?105791\" />');
            loadingImage.insertAfter(pInfScrNode);
            pInfScrNode.hide();
          },
          success: function(data) {
            // remove loading feedback
            pInfScrNode.next().remove();
            var filteredData = $(data).find(".category-products");
          
            filteredData.insertBefore( $("#product-list-foot") );
            $(".product").each(function(aj) {
             
              if ($('#list_view').hasClass('active')){
                $(this).addClass("product-layout-list");
                //console.log('ok');

              }
              else {
                $('#grid_view').addClass("product-layout-grid");
                //console.log('not ok');
              }

              $(this).css('opacity', 1);
              $(this).addClass("item-animated");
              $(this).delay(aj * 100).animate({
                opacity: 1
              }, 500, "easeOutExpo", function() {
                $(this).addClass("item-animated")
              });
            });
            
             
            
            
            loadingImage.remove(); 
            pInfScrLoading = false;
            
           
            
          },
          dataType: "html"
        });
      }
    }
  }
  $(document).ready(function () {
    $(window).scroll(function(){
      $.doTimeout( 'scroll', pInfScrDelay, pInfScrExecute);
      if( $(document).height() - 800 > $(document).scrollTop() + $(window).height() ) {
        pInfScrDelay = 100;
      }
    });
  });
  // https://github.com/cowboy/jquery-dotimeout
